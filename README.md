### Keystore credentials

**Keystore password:** c37c3cb8767041a29ad69ef63c1e49aa
**Key alias:** QGNvb2JyYXN0dXIvYXBwLXZlbmRhcy1jb29icmFzdHVy
**Key password:** aa99381b1a264e38a66251bf2bb1f4e3
**Path:** ./app-vendas-coobrastur.jks

### Expo

- https://expo.io/
- coobrastur
- C00brasti2017

### Google play

- https://play.google.com/console/developers
- ti@coobrastur.com.br
- C00brasti2017

### ⚠️⚠️ Atenção ⚠️⚠️

Antes de subir uma nova versão, é importante mudar o número da versão nos arquivos: **app.json**, **src/screens/Login** e **src/screens/Account**.
Também é necessário alterar a **versionCode** que fica dentro de **app.json**.
Parece algo bobo, mas nos ajuda a organizar melhor o projeto.

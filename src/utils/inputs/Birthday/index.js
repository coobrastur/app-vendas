import React, { useState, useEffect } from "react";
import { View, Text, Picker, StyleSheet } from "react-native";

import { getDaysInMonth, getYear, subYears } from "date-fns";
import ptBr from "date-fns/locale/pt-BR";

const BirthdayIn = ({
  setValue,
  min, // nascimento: 18,
  max, // nascimento: 130,
  label,
}) => {
  const currentDate = new Date();

  const [day, setDay] = useState(currentDate.getUTCDay());
  const [month, setMonth] = useState(currentDate.getUTCMonth());
  const [year, setYear] = useState(currentDate.getUTCFullYear());

  const [days, setDays] = useState([]);
  const [months, setMonths] = useState([]);
  const [years, setYears] = useState([]);

  const getDays = () => {
    const totalDaysInMonth = getDaysInMonth(new Date(year, month));

    let daysArray = [];

    for (let day = 1; day <= totalDaysInMonth; day++) {
      const label = String(day);
      const value = day;

      daysArray.push({ label, value });
    }

    if (day >= totalDaysInMonth) setDay(totalDaysInMonth);

    return setDays(daysArray);
  };

  const getMonths = () => {
    let monthsArray = [];

    for (let month = 0; month < 12; month++) {
      const label = ptBr.localize.month(month, { width: "abbreviated" });
      const value = month;

      monthsArray.push({ label, value });
    }

    return setMonths(monthsArray);
  };

  const getYears = () => {
    const minDate = getYear(subYears(currentDate, max || 130));
    const maxDate = getYear(subYears(currentDate, min || 1));

    let yearsArray = [];

    for (let year = maxDate; year >= minDate; year--) {
      const label = String(year);
      const value = year;

      yearsArray.push({ label, value });
    }

    return setYears(yearsArray);
  };

  useEffect(() => {
    getYears();
    getMonths();
    getDays();
  }, []);

  useEffect(() => {
    getDays();
  }, [month]);

  useEffect(() => {
    const newDate = new Date(year, month, day);
    setValue(newDate);
  }, [day, month, year]);

  const getPickerItems = (data) => {
    return data?.map((item, i) => (
      <Picker.Item
        style={styles.selectDateItem}
        key={i}
        value={item.value}
        label={item.label}
      />
    ));
  };

  return (
    <View style={styles.inputContainer}>
      {label && <Text style={styles.label}>{label}:</Text>}

      <View style={styles.selectDate}>
        <Picker
          style={styles.selectDateItem}
          selectedValue={day}
          onValueChange={(value) => setDay(value)}
        >
          {getPickerItems(days)}
        </Picker>

        <Picker
          style={styles.selectDateItem}
          selectedValue={month}
          onValueChange={(value) => setMonth(value)}
        >
          {getPickerItems(months)}
        </Picker>

        <Picker
          style={styles.selectDateItem}
          selectedValue={year}
          onValueChange={(value) => setYear(value)}
        >
          {getPickerItems(years)}
        </Picker>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    padding: 20,
  },

  label: {
    fontWeight: "bold",
    marginBottom: 10,
    textTransform: "capitalize",
  },

  selectDate: {
    flexDirection: "row",
    backgroundColor: "#fff",
    borderRadius: 5,
  },

  selectDateItem: {
    flex: 1,

    minHeight: 45,
    maxHeight: 120,

    paddingHorizontal: 10,

    overflow: "hidden",
    justifyContent: "center",
  },
});

export default BirthdayIn;

import React, { useState, useEffect } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";

const Tabs = ({ data, color, value, setValue }) => {
  const [tab, setTab] = useState(value || data[0].value);

  useEffect(() => {
    setValue(tab);
  }, [tab]);

  const styles = StyleSheet.create({
    tabs: {
      flexDirection: "row",
      alignItems: "center",
      backgroundColor: color || "transparent",
      paddingTop: 5,
      paddingHorizontal: 20,
    },

    tab: {
      flex: 1,
      paddingHorizontal: 20,
      paddingVertical: 10,
      justifyContent: "center",
      alignItems: "center",
    },

    tabText: {
      color: "#999",
      fontSize: 16,
      fontWeight: "300",
    },

    selectedTab: {
      // borderBottomWidth: 2,
      // borderBottomColor: "#bebebe",
    },

    selectedText: {
      color: "#444",
      fontWeight: "400",
    },
  });

  const getTab = ({ value, label }) => (
    <TouchableOpacity
      key={value}
      style={[styles.tab, tab === value ? styles.selectedTab : {}]}
      onPress={() => setTab(value)}
    >
      <Text style={[styles.tabText, tab === value ? styles.selectedText : {}]}>
        {label}
      </Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.tabs}>
      {data.map((tab) => getTab({ value: tab.value, label: tab.label }))}
    </View>
  );
};

export default Tabs;

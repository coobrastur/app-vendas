export const disableWelcome = () => ({
  type: "DISABLE_WELCOME",
});

export const enableWelcome = () => ({
  type: "ENABLE_WELCOME",
});

import { all } from "redux-saga/effects";

import sign from "./sign/sagas";

function* sagas() {
  yield all([sign()]);
}

export default sagas;

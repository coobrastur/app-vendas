import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

import Welcome from "./src/screens/Welcome";

import Login from "./src/screens/Login";

import Home from "./src/screens/Home";
import Account from "./src/screens/Account";

import ContractsIndex from "./src/screens/Contracts/index";
import ContractsShow from "./src/screens/Contracts/show";
import ContractsUpdate from "./src/screens/Contracts/update";
import ContractsStore from "./src/screens/Contracts/store";

import colors from "./colors.json";

import { useSelector } from "react-redux";

const headerTintColor = "#fff";
const backgroundColor = colors.primary;

const Navigation = () => {
  const welcome = useSelector((state) => state.welcome);
  const sign = useSelector((state) => state.sign);

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={
          sign.data.token ? "home" : welcome.disabled ? "login" : "welcome"
        }
      >
        <Stack.Screen
          name={"welcome"}
          component={Welcome}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name={"login"}
          component={Login}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name={"home"}
          component={Home}
          options={{
            title: "Bem-vindo",
            gestureEnabled: false,
            headerLeft: null,
            headerTintColor,
            headerStyle: { backgroundColor },
            headerTitleStyle: { fontWeight: "bold" },
          }}
        />

        <Stack.Screen
          name={"account"}
          component={Account}
          options={{
            title: "Conta",
            headerTintColor,
            headerStyle: { backgroundColor },
            headerTitleStyle: { fontWeight: "bold" },
          }}
        />

        <Stack.Screen
          name={"contractsIndex"}
          component={ContractsIndex}
          options={{
            title: "Contratos",
            headerTintColor,
            headerStyle: { backgroundColor },
            headerTitleStyle: { fontWeight: "bold" },
          }}
        />

        <Stack.Screen
          name={"contractsShow"}
          component={ContractsShow}
          options={{
            title: "Detalhes",
            headerTintColor,
            headerStyle: { backgroundColor },
            headerTitleStyle: { fontWeight: "bold" },
          }}
        />

        <Stack.Screen
          name={"contractsUpdate"}
          component={ContractsUpdate}
          options={{
            title: "Editar",
            headerTintColor,
            headerStyle: { backgroundColor },
            headerTitleStyle: { fontWeight: "bold" },
          }}
        />

        <Stack.Screen
          name={"contractsStore"}
          component={ContractsStore}
          options={{
            title: "Novo",
            headerTintColor,
            headerStyle: { backgroundColor },
            headerTitleStyle: { fontWeight: "bold" },
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
